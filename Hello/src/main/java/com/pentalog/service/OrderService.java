package com.pentalog.service;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.pentalog.model.Order;

/**
 * The interface OrderService.
 */
public interface OrderService {

	/**
	 * Get order by id.
	 * 
	 * @param id
	 *            - the id
	 * @return order by id.
	 */
	Order getOrderById(int id);

	/**
	 * Get orders by date.
	 * 
	 * @param date
	 *            - the date.
	 * @return orders by date.
	 */
	List<Order> getOrderByDate(Date date);

	/**
	 * Get all orders.
	 * 
	 * @return orders.
	 */
	List<Order> getAllOrders();

	Order saveAndFlush(Order order);

	List<Order> getAll();

}
