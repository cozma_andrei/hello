package com.pentalog.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * The Class GeneralSpringConfig.
 */
@Configuration
@ImportResource({"/META-INF/application-config.xml"})
@ComponentScan(basePackages = { "com.pentalog.dao","com.pentalog.service","com.pentalog.serviceImpl","com.pentalog.rest"})
public class GeneralSpringConfig {
	
	public GeneralSpringConfig(){
	}

	/**
	 * Register the PropertySourcesPlaceholderConfigurer.
	 * 
	 * @return PropertySourcesPlaceholderConfigurer instance
	 */
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

}