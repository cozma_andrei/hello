package com.pentalog.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.pentalog.model.Order;
import com.pentalog.service.OrderService;

@Service
@Path("/orders")
public class OrderWS {

	@Autowired(required=true)
	private OrderService orderService ;

	public OrderService getOrderService() {
		return orderService;
	}

	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}

	@GET
	@Path("/{id}")
	public Response getOrderById(@PathParam("id") int id) {
		Order o = orderService.getOrderById(id);
		String output = "The order date : " + o.getOrderDate().toString();
		System.out.println(output);
		return Response.status(200).entity(output).build();
	}

	@GET
	@Path("/allorders")
	public Response getAllOrders() {
		List<Order> listOfOrders = orderService.getAllOrders();
		String output = "";
		for (Order o : listOfOrders)
			output = "The order date for the order with id " + o.getOrderId()
					+ " is : " + o.getOrderDate().toString() + "\n";
		System.out.println(output);
		return Response.status(200).entity(output).build();
	}


}