package com.pentalog.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.pentalog.model.Order;

/**
 * The interface OrderDAO
 */
public interface OrderDAO extends JpaRepository<Order, Long>{
	
	/**
	 * The constant GET_CUSTOMER_BY_ID.
	 */
	static final String GET_ORDER_BY_ID = "Select * from orders where orderId = :orderId";
	
	static final String GET_ALL_ORDERS = "Select * from orders";
	
	static final String GET_ORDER_BY_DATE = "Select * from orders where orderDate = :orderDate";
	
	/**
	 * Get customer by id.
	 * @param id - the id.
	 * @return customer by id.
	 */
	@Query(GET_ORDER_BY_ID)
	public Order getOrderById(@Param("orderId") int id);
	
	@Query(GET_ALL_ORDERS)
	public List<Order> getAllOrders();
	
	@Query(GET_ORDER_BY_DATE)
	public List<Order> getOrderByDate(@Param("orderDate") Date d);
	
	
	public Order saveOrder(Order o);
}