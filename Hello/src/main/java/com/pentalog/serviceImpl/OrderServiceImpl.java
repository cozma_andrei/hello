package com.pentalog.serviceImpl;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pentalog.dao.OrderDAO;
import com.pentalog.model.Order;
import com.pentalog.service.OrderService;

/**
 * The class OrderServiceImpl.
 */
@Service(value = "orderService")
public class OrderServiceImpl implements OrderService {

	/**
	 * The order DAO.
	 */
	@Autowired
	private OrderDAO orderDao;
	Logger log;

	public OrderDAO getOrderDao() {
		return orderDao;
	}

	public void setOrderDao(OrderDAO orderDao) {
		this.orderDao = orderDao;
	}
	/**
	 * @see OrderService#getOrderById()
	 */
	@Override
	public Order getOrderById(int id) {
		Order returnedOrder = this.orderDao.getOrderById(id);
		System.out.println(returnedOrder.getOrderId());
		log.info(">>" + returnedOrder.getOrderId() + "");
		return returnedOrder;
	}

	/**
	 * @see OrderService#getAllOrders()
	 */
	@Override
	public List<Order> getAllOrders() {
		List<Order> allOrders = this.orderDao.getAllOrders();
		for (Order o : allOrders)
			log.info(">>" + o.getOrderId() + " " + o.getOrderDate());
		return allOrders;
	}

	@Override
	public List<Order> getOrderByDate(Date date) {
		List<Order> returnedOrder = this.orderDao.getOrderByDate(date);
		return returnedOrder;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Order> getAll() {
		List<Order> allOrders = this.orderDao.getAllOrders();
		for (Order o : allOrders)
			log.info(">>" + o.getOrderId() + " " + o.getOrderDate());
		return orderDao.findAll();
	}

	@Override
	public Order saveAndFlush(Order order) {
		if (order != null) {
			order = orderDao.saveAndFlush(order);
		}
		return order;
	}
}
